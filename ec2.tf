# Data source para buscar a AMI mais recente do Ubuntu
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server*"]
  }

  owners = ["099720109477"] # Este é o ID do proprietário da Canonical, a empresa por trás do Ubuntu
}

# Configura o provedor AWS
provider "aws" {
  alias  = "us-east-2" # Alias para o provedor AWS
  region = "us-east-2" # Substitua pela região desejada
}

# Cria uma instância EC2
resource "aws_instance" "devops" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id     = var.vpc_id

  root_block_device {
    encrypted = true
  }

  tags = {
    Name = "devops"
  }
}

# Exemplo de recurso para criar uma instância EC2

# Você pode especificar a AMI (Amazon Machine Image) que deseja usar substituindo o AMI ID.
# Certifique-se de substituir a região no provedor AWS de acordo com a sua preferência.
# Após definir este arquivo, você pode executar "terraform init" e "terraform apply" para criar a instância.

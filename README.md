# Projeto Tofu

Este é o repositório do projeto Tofu, que utiliza hcl para provisionar uma instância EC2 na AWS usando o GitLab CI com pipelines pai-filho.

## Descrição

O objetivo deste projeto é demonstrar como automatizar o provisionamento de recursos na AWS utilizando o Opentofu, integrado ao GitLab CI para automação de pipelines. Utilizaremos pipelines pai-filho para organizar e modularizar nosso fluxo de trabalho.

## Configuração

Antes de começar, é necessário configurar algumas coisas:

1. Certifique-se de ter uma conta na AWS e as credenciais adequadas configuradas.
2. Instale o Opentofu em sua máquina local, se ainda não estiver instalado.
3. Configure as variáveis de ambiente necessárias para o Terraform, como `AWS_ACCESS_KEY_ID` e `AWS_SECRET_ACCESS_KEY`.

## Estrutura do Projeto

- `.gitignore`: Arquivo de configuração do Git para ignorar arquivos e diretórios específicos.
- `.gitlab-ci.yml`: Arquivo de configuração do GitLab CI para definir os pipelines.
- `backend.tf`: Arquivo de configuração do Terraform para definir o backend, como armazenamento de estado remoto.
- `ec2.tf`: Arquivo de configuração do Terraform com a definição da instância EC2.
- `outputs.tf`: Arquivo de configuração do Terraform para definir as saídas dos recursos provisionados.
- `variables.tf`: Arquivo de configuração do Terraform para definir variáveis.
- `versions.tf`: Arquivo de configuração do Terraform para definir as versões dos provedores e módulos.

## Utilização

 Clone este repositório:

```bash
git clone https://gitlab.com/nataliadeassis/tofu.git
```

1. Personalize os arquivos de configuração do Terraform conforme necessário, como variables.tf, ec2.tf, etc.

2. Certifique-se de adicionar arquivos sensíveis ao .gitignore, como arquivos de credenciais, arquivos de estado do Terraform, etc.

3. Envie as alterações para o repositório GitLab.

4. O GitLab CI executará automaticamente os pipelines definidos no arquivo .gitlab-ci.yml. Certifique-se de que as etapas de pipeline estão configuradas conforme necessário para sua infraestrutura.

5. Verifique o status do pipeline no GitLab e monitore a execução.

# Contribuições

Contribuições são bem-vindas! Sinta-se à vontade para enviar pull requests com melhorias, correções de bugs ou novos recursos.
